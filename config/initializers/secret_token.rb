# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = 'e10ab1e6fa40227de8de906a72e2cecd50ee32022d3e49a6158eac3873a9fab912c68ec312b7841cc8e22398bce9ecd0d669d65f6c9428834b22f4bb55cd4710'
